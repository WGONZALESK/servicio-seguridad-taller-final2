pipeline {

	agent any
	
	tools {
	    maven 'Maven 3.8.1'
	    //jdk 'jdk11'
	}
	
	stages {
	
		stage('Compilar') {
			steps {
				echo 'Compilando...'
				sh "mvn clean compile"
			}
		}
		
		stage('Pruebas') {
			steps {
				echo "Ejecutando Pruebas Unitarias/Integrales ..."
				sh "mvn test -Dspring.profiles.active=test"
			}
			post {
				success {
					junit "target/surefire-reports/*.xml"
				}
			}
		}
		
		stage('Análisis de Código Estático') {
			steps {
			    echo 'Analizando código...'
			    
			    withSonarQubeEnv('sonarqube-server') {
			    	sh "mvn sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -Dsonar.projectName=servicio-seguridad -Dsonar.projectVersion=${BUILD_NUMBER}"
			    }
			    
			    timeout(time: 3, unit: 'MINUTES') {
					waitForQualityGate abortPipeline: true
				}

			}
		}

		stage('Build') {
		    steps {
		        echo 'Generando Build...'
		        sh "mvn package -DskipTests=true"
		        sh "docker build -t caltamiranob/servicio-seguridad:${BUILD_NUMBER} ."
		    }
		}
		
		stage('Publicar Imagen') {
            steps {
                echo 'Publicando imagen en repositorio docker-hub'
                script {
                    docker.withRegistry('', 'cnxDockerHub') {
                   		docker.image("caltamiranob/servicio-seguridad:$BUILD_NUMBER").push("$BUILD_NUMBER");
                   		docker.image("caltamiranob/servicio-seguridad:$BUILD_NUMBER").push("latest");
                    }
                }

            }
			post {
			    always {
			        echo 'Eliminar las imagenes generadas localmente'
			        sh "docker image rm caltamiranob/servicio-seguridad:$BUILD_NUMBER"
			    }
			}
        }

	
	}

}
